
#### Linear dependence checks ####




#' wrapper around removeLinDep that removes columns from design matrix if some are found lin dependent
#'
#'
#' Check well-poisedness of design matrix (full rank)
#' No need to remove responses (if any of the predictors is linearly dependent with response, then we have a perfect predictive model => just run a linear regression to get the coefficients)
#' Watch out, the removal or addition of rows may affect the rank of the matrices
#' Thus better to test l.d. for each test and train datasets
#'
#' @param df
#' @param respStr
#'
#' @return
#' @export
#'
#' @examples
makeDesignMatrixWellPoised <- function(df, respStr){


  ncolBefore <- ncol(df )
  df <- removeLinDep (df, respStr)
  ncolAfter <- ncol(df )
  print(paste0("There were ", ncolBefore-ncolAfter   , " linearly dependent columns => removed" ) )


  return(df)
}





#' Identify and remove linearly independent columns in the design matrix
#'
#'
#'  Useful and perhaps even necessarty for glms to avoid numerical issues. Given X in R^{nXp} the design matrix where n>=p.
#'  If rk(X) < p => at least 1 column is a linear combo of the others.
#'  In this case:
#'  1) If rk(X[, -x])==rk(X) for some column x, then column x is l.d. and can be expressed as a linear combo of the other columns in X excluding x.
#'  2) If rk(X[, -x])==rk(X) for ALL columns, then all columns can be expressed as linear combos of the others, Removing ANY column does not affect the rk of X
#'
#'  Once we have identified the l.d. column (in the case of 2, we can take any), then we can perform a linear regression without intercept of the
#'  form x= X[, -x]B and get the non-zeros B's to identify the exact form of the linear dependence.
#'
#'
#'  Not sure why I use the qr decomposition. This is obvisouly inspired by some stack overflow type post + most likely to some speedups obtained by using this
#'  decomposition
#'
#' @param df
#' @param respColStr look for the response in the design matrix and remove it if present. (Finding a linear combo involving the actual response would mean perfect fit, which would be an ideal situation, but this is not the goal of the fct)
#'
#' @return df with P' cols and X=[p1, ..., pP'] has rankd P'
#'
removeLinDep <- function(dfIn, respColStr){

  require(dlyr)
  require(rlist)

  #Remove the response if present
  if(respColStr %in% colnames(dfIn) ) {
    dfResp <- dfIn %>% dplyr::select(one_of(respColStr))
    df <- dfIn %>% dplyr::select(-one_of(respColStr))
  }

  listLinDepCol <- list()

  #This procedure should be used until rankifremoved = rank(M) -1 (we remove a col so full rank is rank -1)
  #Use this recursively + add violating columns to a list + remove these columns from design matrix
  while( ncol(df) != qr(df)$rank ){

    rankifremoved <- sapply(1:ncol(df), function (x) qr(df[,-x])$rank)
    if( max(rankifremoved) == min(rankifremoved) && min(rankifremoved) == ncol(df)-1 ) {print("All cols are lin independent"); break}  # not sure we need to check max(rankifremoved) == ncol(df)-1 ...
    else if ( max(rankifremoved) == min(rankifremoved) ) indicesLinDep <- 1                                                            # all the columns are l.d => take any (the first arbitrarily)
    else indicesLinDep <- which(rankifremoved == max(rankifremoved))[[1]]                                                              # arbitrarily take first one if many

    writeLines( paste0("Watch out, following col is lin dep with others: " , colnames(df)[indicesLinDep] , "\n") )
    listLinDepCol <- list.append(listLinDepCol,colnames(df)[ indicesLinDep ])

    #Find the linear combo by fitting a simple linear reg
    tryCatch({
      lmFit <- lm( formula = paste0( colnames(df)[ indicesLinDep ], "~ . -1"), #no intercept
                   data= df )

      dfCoeff <- data.frame( coeff = round(lmFit$coeff,2),
                             var = names(lmFit$coeff)) %>% dplyr::filter(  abs(coeff) > 10**(-5)  )
      if(nrow(dfCoeff)==0) stop("Warning, can't express ", colnames(df)[ indicesLinDep ],
                                " as linear combo => response is probably a constant - here are distinct values: " , paste(unique(df[[indicesLinDep]]),collapse = ","),
                                " => still removing it")

      print( paste0( colnames(df)[ indicesLinDep ],
                     " = " ,
                     paste(sapply( 1:nrow(dfCoeff), function(k) paste0(dfCoeff$coeff[[k]] , "*" , dfCoeff$var[[k]] )),collapse = " + ")

      )
      )

      writeLines("\n")
    },
    error=function(e) writeLines( paste0("Error finding linear combo in removeLinDep for response ", colnames(dfCoeff)[ indicesLinDep ] , "\n", e))
    )


    #Now remove the offfending column
    df %<>% dplyr::select(-one_of(colnames(df)[ indicesLinDep ]))


  }

  print(paste0("Removed following l.d. columns: ", paste(unlist(listLinDepCol), collapse = ", ")) )


  #Add back the response
  df <- bind_cols(df, dfResp)

  return( df )

}


